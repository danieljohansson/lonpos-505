solutions = importdata('solutions-grouped.txt');
% sol = solutions;
n = length(solutions) / 8;
sol = cell(n, 1);
for i = 1:length(sol)
    start = (i - 1) * 8 + 1;
    sol{i} = solutions(start:start + 7);
end

%%
D = dist_matrix(sol);
figure(1)
imagesc(D)
axis equal

%%
tree = linkage(D, 'complete');
leaf_order = optimalleaforder(tree, D, 'Criteria', 'group');

%{
figure(4)
subplot(2,1,1)
dendrogram(tree)
title('Default Leaf Order')
subplot(2,1,2)
dendrogram(tree, 'reorder', leaf_order)
title('Optimal Leaf Order')
%}

E = dist_matrix(sol(leaf_order));
figure(2)
imagesc(E)
axis equal

%{
order = uint32(leaf_order);
dlmwrite('order.txt', order, '\n');
%}

%%
[s, index] = sort(mean(D));
figure(3)
plot(s, '.')
xlabel('Sorted index')
ylabel('Mean distance')

%{
imwrite(1 - (D / max(D(:))), 'distances.png')
imwrite(1 - (E / max(E(:))), 'distances-sorted.png')
%}

function d = dist(p1, p2)
    n = length(p1);
    D = zeros(n);
    for i = 1:n
        for j = i:n
            D(i, j) = 55 - sum(p1{i} == p2{j});
        end
    end
    D = D + triu(D, 1)';
    d = min(D(:));
end

function D = dist_matrix(x)
    n = length(x);
    D = zeros(n);
    for i = 1:n
        for j = i:n
%             D(i, j) = 55 - sum(x{i} == x{j});
            D(i, j) = dist(x{i}, x{j});
        end
    end
    D = D + triu(D, 1)';
end
