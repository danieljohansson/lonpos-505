extern crate nalgebra;

use std::f64::consts::{FRAC_1_SQRT_2, SQRT_2};
use self::nalgebra::{Cast, Matrix3, Point3, Vector3};

fn cartesian_to_pyramid_coord(x: Point3<f64>) -> Point3<i32> {
    let base_inv = Matrix3::new(
        1.0, 0.0, -FRAC_1_SQRT_2,
        0.0, 1.0, -FRAC_1_SQRT_2,
        0.0, 0.0, SQRT_2
    );
    let x = base_inv * x;
    Point3::new(x.x.round() as i32, x.y.round() as i32, x.z.round() as i32)
}

fn pyramid_to_cartesian_coord(x: Point3<i32>) -> Point3<f64> {
    let base = Matrix3::new(
        1.0, 0.0, 0.5,
        0.0, 1.0, 0.5,
        0.0, 0.0, FRAC_1_SQRT_2
    );
    let x: Point3<f64> = Cast::from(x);
    base * x
}

fn pyramid_coord_in_bounds(p: &Point3<i32>) -> bool {
    p.x >= 0 && p.y >= 0 && p.z >= 0 && p.z < 5 && p.x < 5 - p.z && p.y < 5 - p.z
}

fn pyramid_coord_to_index(p: Point3<i32>) -> Result<usize, String> {
    // If position is outside of pyramid return error.
    if !pyramid_coord_in_bounds(&p) {
        return Err("Position out of bounds".to_owned());
    }
    // Position is inside pyramid.
    // Map transformed coordinates to a linear index.
    let mut i = 0;
    for j in 0..p.z {
        i += (5 - j).pow(2);
    }
    i += (5 - p.z) * p.y + p.x;

    Ok(i as usize)
}

pub fn position_to_index(x: Point3<f64>) -> Result<usize, String> {
    let x_pyramid = cartesian_to_pyramid_coord(x);
    pyramid_coord_to_index(x_pyramid)
}

fn get_pyramid_coords() -> Vec<Point3<i32>> {
    let mut pyramid_coords = Vec::new();
    for k in 0..5 {
        for j in 0..(5 - k) {
            for i in 0..(5 - k) {
                pyramid_coords.push(Point3::new(i, j, k));
            }
        }
    }
    pyramid_coords
}

// All pyramid positions.
pub fn get_positions() -> Vec<Point3<f64>> {
    get_pyramid_coords()
        .into_iter()
        .map(pyramid_to_cartesian_coord)
        .collect()
}

// Neighbors for hole detection.
pub fn get_neighbors() -> Vec<Vec<usize>> {
    get_pyramid_coords()
        .into_iter()
        .map(|co| {
            [
                // Sides
                Vector3::new(1, 0, 0),
                Vector3::new(0, 1, 0),
                Vector3::new(-1, 0, 0),
                Vector3::new(0, -1, 0),
                // Above
                Vector3::new(0, 0, 1),
                Vector3::new(-1, 0, 1),
                Vector3::new(0, -1, 1),
                Vector3::new(-1, -1, 1),
                // Below
                Vector3::new(0, 0, -1),
                Vector3::new(1, 0, -1),
                Vector3::new(0, 1, -1),
                Vector3::new(1, 1, -1)
            ]
            .iter()
            .map(|&offset| co + offset)
            .filter_map(|p| pyramid_coord_to_index(p).ok())
            .collect()
        })
        .collect()
}
