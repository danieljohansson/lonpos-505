
use pieces::{self, Piece, PIECES};
use coordinates;
use pyramid::Pyramid;

fn any_isolated_hole(pyramid: &Pyramid, neighbors: &Vec<Vec<usize>>) -> bool {
    pyramid.into_iter().enumerate().any(|(i, elem)|
        elem.is_none() && neighbors[i].iter().all(
            |&j| pyramid[j].is_some()
        )
    )
}

#[derive(Debug)]
struct State {
    pyramid: Pyramid,
    remaining: Vec<Piece>,
}

pub fn solve(pyramid: Pyramid) -> (Pyramid, u64) {

    // Precompute all possible placements for each piece, and a list of
    // neighbors to every pyramid position.
    let placements = pieces::get_placements();
    let neighbors = coordinates::get_neighbors();

    // Determine remaining pieces and sort them by number of unique placements.
    let mut remaining: Vec<_> = PIECES
        .iter()
        .filter(|x| !pyramid.contains(x))
        .map(|p| *p)
        .collect();
    let num_placements = |p: &Piece| placements.get(p).unwrap().len();
    remaining.sort_by(|a, b| num_placements(b).cmp(&num_placements(a)));

    // Set up stack and initial state.
    let mut iter = 0;
    let mut stack = vec![State {
        pyramid: pyramid.clone(),
        remaining: remaining,
    }];

    // Run search.
    while let Some(State {pyramid, mut remaining}) = stack.pop() {
        let piece = remaining.pop().unwrap();

        iter += 1;

        'place: for indices in placements.get(&piece).unwrap() {
            // If any of the space is occupied already skip to next placement.
            for &i in indices {
                if pyramid[i].is_some() {
                    continue 'place;
                }
            }

            // The piece fits, fill it in to a copy of the pyramid.
            let mut next_pyramid = pyramid.clone();
            for &i in indices {
                next_pyramid[i] = Some(piece);
            }

            // If there are unreachable single holes in the pyramid prune this
            // branch.
            if any_isolated_hole(&next_pyramid, &neighbors) {
                continue 'place;
            }

            // If there are no more pieces remaining then terminate. Otherwise
            // add this version to the stack and continue.
            if remaining.len() == 0 {
                return (next_pyramid, iter);
            } else {
                stack.push(State {
                    pyramid: next_pyramid,
                    remaining: remaining.clone(),
                });
            }
        }
    }

    (pyramid, iter)
}
