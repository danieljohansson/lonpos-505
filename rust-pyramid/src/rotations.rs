extern crate nalgebra;

use std::f64::consts::{FRAC_1_SQRT_2, FRAC_PI_2, PI};
use self::nalgebra::{Rotation3, Vector3};

pub fn get_rotations() -> Vec<Rotation3<f64>> {
    let mut rotations = vec![
        Rotation3::new(Vector3::new(0.0, 0.0, 0.0)),
        Rotation3::new(Vector3::new(0.0, 0.0, FRAC_PI_2)),
        Rotation3::new(Vector3::new(0.0, 0.0, PI)),
        Rotation3::new(Vector3::new(0.0, 0.0, 3.0 * FRAC_PI_2)),
    ];

    let r1 = Rotation3::new(Vector3::new(PI, 0.0, 0.0));
    for i in 0..4 {
        let m = r1 * rotations[i];
        rotations.push(m);
    }
    let s = FRAC_1_SQRT_2 * FRAC_PI_2;
    let r2 = Rotation3::new(Vector3::new(s, s, 0.0));
    for i in 0..8 {
        let m = r2 * rotations[i];
        rotations.push(m);
    }
    let r3 = Rotation3::new(Vector3::new(s, -s, 0.0));
    for i in 0..8 {
        let m = r3 * rotations[i];
        rotations.push(m);
    }

    // The diagonal placements are more bulky and seems to be good to try first.
    rotations.into_iter().rev().collect()
}
