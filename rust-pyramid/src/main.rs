extern crate time;

mod pieces;
mod coordinates;
mod rotations;
mod pyramid;
mod solver;

use pyramid::Pyramid;

fn main() {
    // let init = " JJJJ KKAF KKFF           HHA  HH   H      A       A   ";
    let init = "     C  E C  EEC   ECC  E                              ";
    // let init = "                                                       ";

    let pyr: Pyramid = init.parse().unwrap();
    println!("{}", pyr);

    let t1 = time::precise_time_s();
    let (solution, iter) = solver::solve(pyr);
    let t2 = time::precise_time_s();
    println!("{}", solution);
    println!("Iterations: {}", iter);
    println!("Duration: {} ms", (t2 - t1) * 1000.0);
}
