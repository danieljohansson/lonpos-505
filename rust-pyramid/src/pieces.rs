extern crate nalgebra;

use std::fmt;
use std::collections::HashMap;
use self::nalgebra::Point3;

use rotations;
use coordinates;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub enum Piece {
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
}

pub const PIECES: [Piece; 12] = [
    Piece::A, Piece::B, Piece::C, Piece::D, Piece::E, Piece::F,
    Piece::G, Piece::H, Piece::I, Piece::J, Piece::K, Piece::L,
];

impl fmt::Display for Piece {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match *self {
            Piece::A => "A",
            Piece::B => "B",
            Piece::C => "C",
            Piece::D => "D",
            Piece::E => "E",
            Piece::F => "F",
            Piece::G => "G",
            Piece::H => "H",
            Piece::I => "I",
            Piece::J => "J",
            Piece::K => "K",
            Piece::L => "L",
        };
        write!(f, "{}", s)
    }
}

impl Piece {
    pub fn from_char(c: char) -> Option<Piece> {
        match c {
            'A' => Some(Piece::A),
            'B' => Some(Piece::B),
            'C' => Some(Piece::C),
            'D' => Some(Piece::D),
            'E' => Some(Piece::E),
            'F' => Some(Piece::F),
            'G' => Some(Piece::G),
            'H' => Some(Piece::H),
            'I' => Some(Piece::I),
            'J' => Some(Piece::J),
            'K' => Some(Piece::K),
            'L' => Some(Piece::L),
            _ => None,
        }
    }
}

 fn shape(p: Piece) -> Vec<Point3<f64>> {
    match p {
        Piece::A => vec![[0., 0.], [1., 0.], [2., 0.], [0., 1.]],
        Piece::B => vec![[0., 0.], [1., 0.], [2., 0.], [0., 1.], [1., 1.]],
        Piece::C => vec![[0., 0.], [1., 0.], [2., 0.], [3., 0.], [0., 1.]],
        Piece::D => vec![[0., 0.], [1., 0.], [2., 0.], [3., 0.], [1., 1.]],
        Piece::E => vec![[0., 0.], [1., 0.], [2., 0.], [2., 1.], [3., 1.]],
        Piece::F => vec![[0., 0.], [1., 0.], [0., 1.]],
        Piece::G => vec![[0., 0.], [1., 0.], [2., 0.], [0., 1.], [0., 2.]],
        Piece::H => vec![[0., 0.], [1., 0.], [1., 1.], [2., 1.], [2., 2.]],
        Piece::I => vec![[0., 0.], [1., 0.], [2., 0.], [0., 1.], [2., 1.]],
        Piece::J => vec![[0., 0.], [1., 0.], [2., 0.], [3., 0.]],
        Piece::K => vec![[0., 0.], [1., 0.], [0., 1.], [1., 1.]],
        Piece::L => vec![[0., 0.], [1., 0.], [2., 0.], [1., -1.], [1., 1.]],
    }
    .iter()
    .map(|xy| Point3::new(xy[0], xy[1], 0.0)).collect()
}

// Precomputation of every valid placement in the pyramid for each piece to
// avoid matrix multiplicaition and bounds checking during the search.
pub fn get_placements() -> HashMap<Piece, Vec<Vec<usize>>> {
    let rotations = rotations::get_rotations();
    let positions = coordinates::get_positions();

    let mut placements = HashMap::new();

    for &piece in &PIECES {
        let mut list = Vec::new();
        let shape = shape(piece);

        for &rot in &rotations {
            'pos: for pos in &positions {
                let mut indices = Vec::new();

                // Rotate, translate, and convert to index.
                for &p in &shape {
                    let q = rot * p + pos.to_vector();

                    if let Ok(i) = coordinates::position_to_index(q) {
                        indices.push(i);
                    } else {
                        // If sphere is out-of-bounds skip to next position.
                        continue 'pos;
                    }
                }

                // Make sure the indices are sorted.
                indices.sort();

                // If placement is a duplicate skip to next position.
                for placement in &list {
                    if *placement == indices {
                        continue 'pos;
                    }
                }

                // Otherwise add placement to list.
                list.push(indices);
            }
        }
        placements.insert(piece, list);
    }
    placements
}
