use std;
use std::fmt;
use pieces::Piece;

pub struct Pyramid([Option<Piece>; 55]);

impl std::ops::Index<usize> for Pyramid {
    type Output = Option<Piece>;
    fn index(&self, index: usize) -> &Option<Piece> {
        &self.0[index]
    }
}

impl std::ops::IndexMut<usize> for Pyramid {
    fn index_mut(&mut self, index: usize) -> &mut Option<Piece> {
        &mut self.0[index]
    }
}

impl Clone for Pyramid {
    fn clone(&self) -> Pyramid {
        Pyramid(self.0)
    }
}

impl<'a> std::iter::IntoIterator for &'a Pyramid {
    type IntoIter = std::slice::Iter<'a, Option<Piece>>;
    type Item = &'a Option<Piece>;
    fn into_iter(self) -> Self::IntoIter {
        (&self.0 as &[Option<Piece>]).into_iter()
    }
}

impl fmt::Debug for Pyramid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        fmt::Debug::fmt(&&self.0[..], f)
    }
}

impl std::str::FromStr for Pyramid {
    type Err = ();

    fn from_str(s: &str) -> Result<Pyramid, ()> {
        if s.len() != 55 {
            Err(())
        } else {
            let mut arr = [None; 55];
            for (i, c) in s.chars().enumerate() {
                arr[i] = Piece::from_char(c);
            }
            Ok(Pyramid(arr))
        }
    }
}

impl fmt::Display for Pyramid {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s: String = self
            .into_iter()
            .map(|&elem| if elem.is_some() {
                format!("{}", elem.unwrap())
            } else {
                "_".to_owned()
            })
            .collect();
        write!(f, "{}", s)
    }
}

impl Pyramid {
    pub fn contains(&self, x: &Piece) -> bool {
        self.into_iter().any(|&y| Some(*x) == y)
    }
}
