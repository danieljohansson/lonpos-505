function rot_matrix(axis, ang) {
    let cos = Math.cos(ang * Math.PI / 180);
    let sin = Math.sin(ang * Math.PI / 180);
    let mag = Math.sqrt(axis[0] * axis[0] + axis[1] * axis[1] + axis[2] * axis[2]);
    let ux = axis[0] / mag;
    let uy = axis[1] / mag;
    let uz = axis[2] / mag;

    let r11 = cos + ux * ux * (1 - cos);
    let r13 = ux * uz * (1 - cos) + uy * sin;
    let r12 = ux * uy * (1 - cos) - uz * sin;

    let r21 = uy * ux * (1 - cos) + uz * sin;
    let r22 = cos + uy * uy * (1 - cos);
    let r23 = uy * uz * (1 - cos) - ux * sin;

    let r31 = uz * ux * (1 - cos) - uy * sin;
    let r32 = uz * uy * (1 - cos) + ux * sin;
    let r33 = cos + uz * uz * (1 - cos);

    return [
        [r11, r12, r13],
        [r21, r22, r23],
        [r31, r32, r33],
    ];
}

let rotations = [
    rot_matrix([0, 0, 1], 0),
    rot_matrix([0, 0, 1], 90),
    rot_matrix([0, 0, 1], 180),
    rot_matrix([0, 0, 1], 270),
];

for (let i = 0; i < 4; i++) {
    rotations.push(numeric.dot(rot_matrix([1, 0, 0], 180), rotations[i]));
}
for (let i = 0; i < 8; i++) {
    rotations.push(numeric.dot(rot_matrix([1, 1, 0], 90), rotations[i]));
}
for (let i = 0; i < 8; i++) {
    rotations.push(numeric.dot(rot_matrix([1, -1, 0], 90), rotations[i]));
}

// The diagonal placements are more bulky and seems to be good to try first.
rotations.reverse();
