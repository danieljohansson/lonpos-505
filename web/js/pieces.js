let pieces = {
    A: {
        color: 0xf07e1b,
        shape: [[0, 0], [1, 0], [2, 0], [0, 1]],
    },
    B: {
        color: 0xe71e16,
        shape: [[0, 0], [1, 0], [2, 0], [0, 1], [1, 1]],
    },
    C: {
        color: 0x046bad,
        shape: [[0, 0], [1, 0], [2, 0], [3, 0], [0, 1]],
    },
    D: {
        color: 0xf7c2be,
        shape: [[0, 0], [1, 0], [2, 0], [3, 0], [1, 1]],
    },
    E: {
        color: 0x019942,
        shape: [[0, 0], [1, 0], [2, 0], [2, 1], [3, 1]],
    },
    F: {
        color: 0xf2f0d7,
        shape: [[0, 0], [1, 0], [0, 1]],
    },
    G: {
        color: 0x6ec1ed,
        shape: [[0, 0], [1, 0], [2, 0], [0, 1], [0, 2]],
    },
    H: {
        color: 0xda538e,
        shape: [[0, 0], [1, 0], [1, 1], [2, 1], [2, 2]],
    },
    I: {
        color: 0xfff300,
        shape: [[0, 0], [1, 0], [2, 0], [0, 1], [2, 1]],
    },
    J: {
        color: 0x86196c,
        shape: [[0, 0], [1, 0], [2, 0], [3, 0]],
    },
    K: {
        color: 0x98cc3c,
        shape: [[0, 0], [1, 0], [0, 1], [1, 1]],
    },
    L: {
        color: 0xa9a9a9,
        shape: [[0, 0], [1, 0], [2, 0], [1, -1], [1, 1]],
    },
};
// Make shape matrices 3 x n.
for (let key of Object.keys(pieces)) {
    let s = pieces[key].shape;
    for (let i = 0; i < s.length; i++) {
        // Add z value.
        s[i].push(0);
    }
    pieces[key].shape = numeric.transpose(s);
}
