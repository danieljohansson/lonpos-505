let inits = {};

inits['2-happy'] = [
    'JJJJD',
    'EKLDD',
    'AAALD',
    'BBAFD',
    'BBBFF',

    'K   ',
    'EKL ',
    ' E  ',
    '    ',

    'KL ',
    'E L',
    ' E ',

    '  ',
    '  ',

    ' ',
].join('');

inits['1-happy'] = [
    'JJJJD',
    '  LDD',
    '   LD',
    'AEEED',
    'AAAEE',

    ' III',
    ' ILI',
    '  KK',
    '  KK',

    ' L ',
    '  L',
    '   ',

    '  ',
    '  ',

    ' ',
].join('');

inits['1-angry'] = [
    ' JJJJ',
    ' KKAF',
    ' KKFF',
    '     ',
    '     ',

    ' HHA',
    '  HH',
    '   H',
    '    ',

    '  A',
    '   ',
    '   ',

    ' A',
    '  ',

    ' ',
].join('');

inits['2-angry'] = [
    ' D   ',
    'CD  H',
    'CDDHH',
    'CDHHA',
    'CCAAA',

    ' E  ',
    'EE  ',
    'EI I',
    'EIII',

    '   ',
    '   ',
    '   ',

    '  ',
    '  ',

    ' ',
].join('');

inits['3-angry'] = [
    '    C',
    '  KKC',
    'IIKKC',
    'I ACC',
    'IIAAA',

    '    ',
    '    ',
    '    ',
    '    ',

    '   ',
    '   ',
    '   ',

    '  ',
    '  ',

    ' ',
].join('');

inits['4-angry'] = [
    ' CCCC',
    'D   C',
    'DD  G',
    'D   G',
    'D GGG',

    '    ',
    '    ',
    '    ',
    '    ',

    '   ',
    '   ',
    '   ',

    '  ',
    '  ',

    ' ',
].join('');

inits['5-angry'] = [
    '     ',
    'C  E ',
    'C  EE',
    'C   E',
    'CC  E',

    '    ',
    '    ',
    '    ',
    '    ',

    '   ',
    '   ',
    '   ',

    '  ',
    '  ',

    ' ',
].join('');

inits['blank'] = [
    '     ',
    '     ',
    '     ',
    '     ',
    '     ',

    '    ',
    '    ',
    '    ',
    '    ',

    '   ',
    '   ',
    '   ',

    '  ',
    '  ',

    ' ',
].join('');
