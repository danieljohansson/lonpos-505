
// Scene and camera
let scene = new THREE.Scene();
let camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 500);
camera.position.set(5, 4, 8);

// Renderer
let renderer = new THREE.WebGLRenderer({
    antialias: true,
});
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth - 10, window.innerHeight - 10);
document.body.appendChild(renderer.domElement);

// Orbit controls
controls = new THREE.OrbitControls(camera, renderer.domElement);
controls.target.set(2, 1, 2)
camera.lookAt(new THREE.Vector3(2, 1, 2));

// Axes
// axes = new THREE.AxisHelper(6);
// scene.add(axes);

// Lights
let lights = [];
lights[0] = new THREE.AmbientLight(0x505050);
lights[1] = new THREE.PointLight(0xffffff, 0.50, 0);
lights[2] = new THREE.PointLight(0xffffff, 0.25, 0);
lights[3] = new THREE.PointLight(0xffffff, 0.13, 0);

lights[1].position.set(0, 200, 0);
lights[2].position.set(100, 200, 100);
lights[3].position.set(-100, -200, -100);

scene.add(lights[0]);
scene.add(lights[1]);
scene.add(lights[2]);
scene.add(lights[3]);

// Render loop
function render() {
    requestAnimationFrame(render);
    renderer.render(scene, camera);
}
render();

window.addEventListener('resize', function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth - 10, window.innerHeight - 10);
}, false);

// ------------------------------------------------------------------

function add_sphere(opt) {
    let geometry = new THREE.SphereGeometry(0.5, 22, 16);
    let material = new THREE.MeshPhongMaterial({
        color: opt.color,
        emissive: 0x101010,
        transparent: !!opt.transparent,
        opacity: 0.15,
        // side: THREE.DoubleSide,
        shading: THREE.FlatShading
        // shading: THREE.SmoothShadikng
    });
    let sphere = new THREE.Mesh(geometry, material);
    sphere.position.x = opt.pos.x;
    sphere.position.y = opt.pos.y;
    sphere.position.z = opt.pos.z;
    scene.add(sphere);
    return sphere;
}

// Draw transparent pyramid.
let pyramid_spheres = [];
for (let i = 0; i < positions.length; i++) {
    let p = index_to_position(i);
    pyramid_spheres[i] = add_sphere({
        pos: {
            x: p[0],
            z: p[1],
            y: p[2],
        },
        color: 0xaaaaaa,
        transparent: true,
    });
}

function update_pyramid(pyramid) {
    for (let i = 0; i < pyramid.length; i++) {
        if (pieces.hasOwnProperty(pyramid[i])) {
            pyramid_spheres[i].material.transparent = false;
            let color = pieces[pyramid[i]].color;
            pyramid_spheres[i].material.color.setHex(color);
        } else {
            pyramid_spheres[i].material.transparent = true;
            pyramid_spheres[i].material.color.setHex(0xaaaaaa);
        }
    }
}

// ------------------------------------------------------------------

let init_selector = document.getElementById('init-config');
let info_text = document.getElementById('info');
let run_button = document.getElementById('run');

let pyramid = inits[init_selector.value];
update_pyramid(pyramid);

init_selector.addEventListener('change', function () {
    let pyramid = inits[init_selector.value];
    update_pyramid(pyramid);
    info_text.textContent = '';
});

run_button.addEventListener('click', function () {
    let pyramid = inits[init_selector.value];
    worker.postMessage(pyramid);
});

// Set up worker.
let worker = new Worker('js/pyramid-worker.js');
worker.onmessage = function (message) {
    let data = message.data;
    if (data.hasOwnProperty('pyramid')) {
        update_pyramid(data.pyramid);
    }
    if (data.hasOwnProperty('iterations')) {
        info_text.textContent = 'Iterations: ' + data.iterations;
    }
}
