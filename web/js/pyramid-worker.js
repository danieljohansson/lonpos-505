importScripts('numeric-1.2.6.js', 'coordinates.js', 'pieces.js', 'rotations.js');

// Precomputation of every valid placement in the pyramid for each piece to
// avoid matrix multiplicaition and bounds checking during the search.
performance.mark('start_pre');

for (let key of Object.keys(pieces)) {
    pieces[key].placements = [];
    let s = pieces[key].shape;

    for (let r of rotations) {
        pos: for (let p of positions) {
            // Rotate, translate, and convert to index.
            let s_rot = numeric.transpose(numeric.dot(r, s));
            let i = [];
            for (let m = 0; m < s_rot.length; m++) {
                let s_rot_transl = numeric.add(s_rot[m], p);
                try {
                    i[m] = position_to_index(s_rot_transl);
                } catch (e) {
                    // If sphere is out-of-bounds skip to next position.
                    continue pos;
                }
            }

            // Make sure the indices are sorted.
            i.sort();

            // If placement is a duplicate skip to next position.
            let current = pieces[key].placements;
            for (let j = 0; j < current.length; j++) {
                let same = current[j].every((elem, m) => elem == i[m]);
                if (same) {
                    continue pos;
                }
            }

            // Otherwise add placement to list.
            pieces[key].placements.push(i);
        }
    }
}

performance.mark('end_pre');
performance.measure('pre', 'start_pre', 'end_pre');
console.log('precomputation', performance.getEntriesByName('pre')[0].duration);

function any_isolated_hole(pyramid) {
    return pyramid.some((elem, i) =>
        elem == ' ' && neighbors[i].every(j => pyramid[j] != ' ')
    );
}

onmessage = function (message) {

    performance.mark('start_pyramid');

    let pyramid = message.data.split('');

    // Determine remaining pieces and sort them by number of unique placements.
    let remaining = Object.entries(pieces)
        .filter(x => pyramid.indexOf(x[0]) == -1)
        .sort((a, b) => b[1].placements.length - a[1].placements.length)
        .map(x => x[0]);

    let iter = 0;
    let stack = [];
    stack.push({pyramid, remaining});

    while (stack.length) {
        let {pyramid, remaining} = stack.pop();
        let piece = remaining.pop();
        let s = pieces[piece].shape;

        // Post updates periodically.
        if (iter % 10000 == 0) {
            postMessage({
                pyramid: pyramid.join(''),
                iterations: iter,
            });
        }
        iter++;

        place: for (let i of pieces[piece].placements) {
            // If any of the space is occupied already skip to next placement.
            for (let m = 0; m < i.length; m++) {
                if (pyramid[i[m]] != ' ') {
                    continue place;
                }
            }

            // The piece fits, fill it in to a copy of the pyramid.
            let next_pyramid = pyramid.slice();
            for (let m = 0; m < i.length; m++) {
                next_pyramid[i[m]] = piece;
            }

            // If there are unreachable single holes in the pyramid prune this
            // branch.
            if (any_isolated_hole(next_pyramid)) {
                continue place;
            }

            // If there are no more pieces remaining then terminate. Otherwise
            // add this version to the stack and continue.
            if (remaining.length == 0) {
                console.log('yay!');
                stack.length = 0;
                postMessage({
                    pyramid: next_pyramid.join(''),
                    iterations: iter,
                });
                break place;
            } else {
                stack.push({
                    pyramid: next_pyramid,
                    remaining: remaining.slice(),
                });
            }
        }
    }

    performance.mark('end_pyramid');
    performance.measure('pyramid', 'start_pyramid', 'end_pyramid');
    console.log('pyramid:', performance.getEntriesByName('pyramid')[0].duration);
}
