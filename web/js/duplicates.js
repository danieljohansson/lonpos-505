// Compute an array of equivalent solutions to the solution s0. These are
// rotations and mirror images.
function equivalents(s0) {
    let pre = s => s
        .split('')
        .map((_, i) => index_to_position(i))
        .map(pos => numeric.add(pos, [-2, -2, 0]))
    let post = s => s
        .map(pos => numeric.add(pos, [2, 2, 0]))
        .map(pos => position_to_index(pos))

    let mirror = [[-1, 0, 0], [0, 1, 0], [0, 0, 1]];
    let transforms = [
        rot_matrix([0, 0, 1], 90),
        rot_matrix([0, 0, 1], 180),
        rot_matrix([0, 0, 1], 270),
        mirror,
        numeric.dot(rot_matrix([0, 0, 1], 90), mirror),
        numeric.dot(rot_matrix([0, 0, 1], 180), mirror),
        numeric.dot(rot_matrix([0, 0, 1], 270), mirror),
    ];

    return transforms.map(tr => {
        let perm = post(pre(s0).map(pos => numeric.dot(tr, pos)));
        let s1 = [];
        for (let i = 0; i < s0.length; i++) {
            s1[i] = s0[perm[i]];
        }
        return s1.join('');
    });
}

function deduplicate(solutions) {
    for (let i = 0; i < solutions.length; i++) {
        let equivs = equivalents(solutions[i]);
        for (let j = i; j < solutions.length; j++) {
            for (let d of equivs) {
                if (d == solutions[j]) {
                    solutions.splice(j, 1);
                    j--;
                    break;
                }
            }
        }
    }
}

function is_unique_solution(candidate, solutions) {
    let equivs = equivalents(candidate);
    for (let sol of solutions) {
        if (sol == candidate) {
            return false;
        }
        for (let eq of equivs) {
            if (sol == eq) {
                return false;
            }
        }
    }
    return true;
}
