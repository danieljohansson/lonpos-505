let sol = [];

function make_eq_groups(solutions) {
    eq_groups = [];

    while (solutions.length) {
        let s = solutions.pop();
        if (!eq_groups.includes(s)) {
            eq_groups.push(s);
        }
        let eq = equivalents(s);
        for (let i = 0; i < eq.length; i++) {
            if (!eq_groups.includes(eq[i])) {
                eq_groups.push(eq[i]);
            }
        }
    }

    return eq_groups;
}

fetch('solutions.json')
    .then(function (response) {
        return response.json();
    })
    .then(function (json) {
        let eqg = make_eq_groups(json);
        console.log(eqg);
        sol = eqg;
    });

function clipboard(text) {
    let textarea = document.createElement('textarea');
    textarea.style.opacity = 0;
    document.body.appendChild(textarea);
    textarea.value = text;
    textarea.select();
    let status = document.execCommand('copy');
    textarea.remove();
    return status;
}

document.body.addEventListener('click', function () {
    try {
        var successful = clipboard(sol.join('\n'));
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text command was ' + msg);
    } catch (err) {
        console.log('Oops, unable to copy');
    }
});
