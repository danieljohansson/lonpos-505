function cartesian_to_pyramid_coord(x) {
    // let base_inv = [
    //     [1, 0, -1 / Math.SQRT2],
    //     [0, 1, -1 / Math.SQRT2],
    //     [0, 0, Math.SQRT2],
    // ];
    // return numeric.dot(base_inv, x).map(Math.round);
    return [
        Math.round(x[0] - x[2] / Math.SQRT2),
        Math.round(x[1] - x[2] / Math.SQRT2),
        Math.round(x[2] * Math.SQRT2),
    ];
}
function pyramid_to_cartesian_coord(x) {
    let base = [
        [1, 0, 0.5],
        [0, 1, 0.5],
        [0, 0, 1 / Math.SQRT2],
    ];
    return numeric.dot(base, x);
}
function pyramid_coord_in_bounds(x) {
    return x[0] >= 0 && x[1] >= 0 && x[2] >= 0 &&
        x[2] < 5 &&
        x[0] < 5 - x[2] &&
        x[1] < 5 - x[2];
}
function pyramid_coord_to_index(x) {
    // If position is outside of pyramid return error.
    if (!pyramid_coord_in_bounds(x)) {
        throw new RangeError('Position out of bounds');
    }
    // Position is inside pyramid.
    // Map transformed coordinates to a linear index.
    let i = 0;
    for (let j = 0; j < x[2]; j++) {
        i += (5 - j) * (5 - j);
    }
    i += (5 - x[2]) * x[1] + x[0];

    return i;
}

// Make table of all pyramid positions.
let pyramid_coords = [];
for (let k = 0; k < 5; k++) {
    for (let j = 0; j < 5 - k; j++) {
        for (let i = 0; i < 5 - k; i++) {
            pyramid_coords.push([i, j, k]);
        }
    }
}
let positions = pyramid_coords.map(pyramid_to_cartesian_coord);

function index_to_position(i) {
    return positions[i];
}
function position_to_index(x) {
    let x_pyramid = cartesian_to_pyramid_coord(x);
    return pyramid_coord_to_index(x_pyramid);
}

// Compute neighbors for hole detection.
let neighbors = pyramid_coords.map(x_p =>
    [
        // Sides
        [ 1,  0, 0],
        [ 0,  1, 0],
        [-1,  0, 0],
        [ 0, -1, 0],
        // Above
        [ 0,  0, 1],
        [-1,  0, 1],
        [ 0, -1, 1],
        [-1, -1, 1],
        // Below
        [0, 0, -1],
        [1, 0, -1],
        [0, 1, -1],
        [1, 1, -1],
    ]
    .map(offset => numeric.add(x_p, offset))
    .filter(pyramid_coord_in_bounds)
    .map(pyramid_coord_to_index)
);

